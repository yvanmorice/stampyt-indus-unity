﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public enum SensorType
{
    MicroFourThirds,
    APS_C,
    FullFrame
}

public class CameraSwitchBehavior : MonoBehaviour
{
    public Camera[] cameras;
    private int currentCameraIndex;
    private GameObject delorean;
    private GameObject peugeot508;
    private SensorType _currentSensorType;

    public SensorType CurrentSensorType
    {
        get => _currentSensorType;
        set
        {
            _currentSensorType = value;
            this.SetCameraProperties(value);
        }
    }

    // Use this for initialization
    void Start()
    {
        this.delorean = GameObject.Find("Delorean");
        this.delorean.SetActive(false);
        this.peugeot508 = GameObject.Find("Peugeot_508");

        Debug.Log("Script initialized");
        this.cameras = Camera.allCameras.OrderBy(_ => _.name).ToArray();
        currentCameraIndex = 0;

        //Turn all cameras off, except the first default one
        for (int i = 0; i < cameras.Length; i++)
        {
            cameras[i].gameObject.SetActive(i == 0);
        }

        this.CurrentSensorType = SensorType.FullFrame;

    }

    private Vector2 GetSensorSize(SensorType type)
    {
        switch (type)
        {
            default:
            case SensorType.MicroFourThirds:
                return new Vector2(17.3f, 13f);
            case SensorType.APS_C:
                return new Vector2(22.2f, 14.8f);
            case SensorType.FullFrame:
                return new Vector2(36f, 24f);
        }
    }

    private float GetFocalLength(SensorType sensorType, int cameraIndex)
    {
        switch (sensorType)
        {
            default:
            case SensorType.MicroFourThirds:
                return this.cameras.Length == 10 && (cameraIndex == 2 || cameraIndex == 7) ? 9 : 12;
            case SensorType.APS_C:
                return this.cameras.Length == 10 && (cameraIndex == 2 || cameraIndex == 7) ? 10 : 14;
            case SensorType.FullFrame:
                return this.cameras.Length == 10 && (cameraIndex == 2 || cameraIndex == 7) ? 16 : 24;

        }
    }

    private void SetCameraProperties(SensorType type)
    {
        var sensorSize = GetSensorSize(type);

        for (int i = 0; i < cameras.Length; i++)
        {
            cameras[i].sensorSize = sensorSize;
            cameras[i].focalLength = GetFocalLength(type, i);
        }
    }

    void OnMouseDown()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //If the c button is pressed, switch to the next camera
        //Set the camera at the current index to inactive, and set the next one in the array to active
        //When we reach the end of the camera array, move back to the beginning or the array.

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            currentCameraIndex++;
            Debug.Log("Right arrow has been pressed. Switching to the next camera");
            if (currentCameraIndex < cameras.Length)
            {
                cameras[currentCameraIndex - 1].gameObject.SetActive(false);
                cameras[currentCameraIndex].gameObject.SetActive(true);
            }
            else
            {
                cameras[currentCameraIndex - 1].gameObject.SetActive(false);
                currentCameraIndex = 0;
                cameras[currentCameraIndex].gameObject.SetActive(true);
            }
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            currentCameraIndex--;
            Debug.Log("Right arrow has been pressed. Switching to the next camera");
            if (currentCameraIndex >= 0)
            {
                cameras[currentCameraIndex + 1].gameObject.SetActive(false);
                cameras[currentCameraIndex].gameObject.SetActive(true);
            }
            else
            {
                cameras[currentCameraIndex + 1].gameObject.SetActive(false);
                currentCameraIndex = cameras.Length - 1;
                cameras[currentCameraIndex].gameObject.SetActive(true);
            }
        }
        else if (Input.GetKeyDown(KeyCode.M))
        {
            if (this.peugeot508.activeSelf)
            {
                this.peugeot508.SetActive(false);
                this.delorean.SetActive(true);
            }
            else if (this.delorean.activeSelf)
            {
                this.peugeot508.SetActive(false);
                this.delorean.SetActive(false);
            }
            else
            {
                this.peugeot508.SetActive(true);
                this.delorean.SetActive(false);
            }
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            ScreenCapture.CaptureScreenshot($"Camera_Export_{currentCameraIndex.ToString().PadLeft(2, '0')}.png");
            Debug.Log("Screenshot taken");
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            switch (this.CurrentSensorType)
            {
                case SensorType.MicroFourThirds:
                    this.CurrentSensorType = SensorType.APS_C;
                    break;
                case SensorType.APS_C:
                    this.CurrentSensorType = SensorType.FullFrame;
                    break;
                case SensorType.FullFrame:
                    this.CurrentSensorType = SensorType.MicroFourThirds;
                    break;
            }
        }
    }
}